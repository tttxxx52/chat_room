package config

import (
	"github.com/gin-gonic/gin"
	"os"
)

type configDB struct {
	Host     string
	Port     string
	DBName   string
	Username string
	Password string
}

type configMongoDB struct {
	Host     string
	Port     string
	DBName   string
	Username string
	Password string
}

type configRedis struct {
	Host     string
	Port     string
	DBName   string
	Password string
}

//Global Config Variable
var DataBaseConfig 		configDB
var MongoConfig 		configMongoDB
var RedisConfig		configRedis

func databaseConfig() {
	switch gin.Mode() {
	case gin.ReleaseMode:
		DataBaseConfig = configDB{
			Host:     "127.0.0.1",
			Port:     "3306",
			DBName:   "chat_test",
			Username: "",
			Password: ""}

		MongoConfig = configMongoDB{
			Host:     "127.0.0.1",
			Port:     "27017",
			DBName:   "chat_test",
			Username: "",
			Password: ""}

		RedisConfig= configRedis{
			"192.168.0.100",
			"6379",
			"0",
			""}


	case gin.DebugMode:
		DataBaseConfig = configDB{
			os.Getenv("DB_HOST"),
			os.Getenv("DB_POST"),
			os.Getenv("DB_NAME"),
			os.Getenv("DB_USER"),
			os.Getenv("DB_PASSWORD")}

		MongoConfig = configMongoDB{
			os.Getenv("MongoDB_HOST"),
			os.Getenv("MongoDB_POST"),
			os.Getenv("MongoDB_NAME"),
			os.Getenv("MongoDB_USER"),
			os.Getenv("MongoDB_PASSWORD")}

		RedisConfig= configRedis{
			os.Getenv("Redis_HOST"),
			os.Getenv("Redis_POST"),
			os.Getenv("Redis_NAME"),
			os.Getenv("Redis_PASSWORD")}

	case gin.TestMode:
		DataBaseConfig = configDB{
			Host:     "127.0.0.1",
			Port:     "3306",
			DBName:   "chat_test",
			Username: "",
			Password: ""}

		MongoConfig = configMongoDB{
			Host:     "127.0.0.1",
			Port:     "27017",
			DBName:   "chat_test",
			Username: "",
			Password: ""}

		RedisConfig= configRedis{
			"192.168.0.100",
			"6379",
			"0",
			""}
	}
}
