package content

import (
	"api/util/log"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"net/http"
)

var wsUpgrade = websocket.Upgrader{
	ReadBufferSize:  4096,
	WriteBufferSize: 4096,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type Websocket struct {
	Conn *websocket.Conn
	Request *http.Request
}

func NewConn(c *gin.Context) (*websocket.Conn,*http.Request, error) {
	conn, err := wsUpgrade.Upgrade(c.Writer, c.Request, nil)
	request := c.Request
	log.Error(err)
	return conn,request, err
}