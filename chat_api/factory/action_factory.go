package factory

import "api/handler"

var (
	channelHandler handler.ChannelHandler
	authHandler    handler.AuthHandler
	userHandler    handler.UserHandler
	msgLogHandler  handler.MsgLogHandler
)

var ActionFactoryAuth = map[string]interface{}{
	//======================================================================
	//						authHandler Api
	//======================================================================
	"GetChannelId": &channelHandler,

	//======================================================================
	//						userHandler Api
	//======================================================================
	"GetFriendList": &userHandler,

	//======================================================================
	//						msgLogHandler Api
	//======================================================================
	"GetMsgLog": &msgLogHandler,
	"GetUnReadCountList": &msgLogHandler,
	"ReadMessage": &msgLogHandler,

}

var ActionFactory = map[string]interface{}{
	//======================================================================
	//						Not Auth Handler Api
	//======================================================================
	"SignIn": &authHandler,
}
