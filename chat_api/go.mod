module api

go 1.14

require (
	github.com/Shopify/sarama v1.26.4
	github.com/dgryski/dgoogauth v0.0.0-20190221195224-5a805980a5f3
	github.com/disintegration/imaging v1.6.2
	github.com/garyburd/redigo v1.6.2
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/websocket v1.4.2
	github.com/hashicorp/go-uuid v1.0.2
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/pkg/errors v0.9.1
	github.com/satori/go.uuid v1.2.0
	github.com/zeekay/gochimp3 v0.0.0-20200626033335-34f0aa1c44a4
	go.mongodb.org/mongo-driver v1.4.0
	golang.org/x/text v0.3.3
)
