package handler

import (
	"api/content"
	"api/model"
	"api/util"
	"api/util/crypto"
	"api/util/log"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"strconv"
	"time"
)
type AuthHandler content.Handler

func (handler *AuthHandler) SignIn(c *gin.Context) interface{} {
	type dataReceived struct {
		Account  string `json:"account"`
		Password string `json:"password"`
	}
	var data dataReceived
	var users model.Users
	paramStr, _ := crypto.KeyDecrypt(c.GetString("parameters"))
	if err := json.Unmarshal([]byte(paramStr), &data); err != nil {
		log.Error(err)
		return util.RS{Message: "", Status: false}
	} else if data.Account == "" {
		return util.RS{Message: "請輸入帳號", Status: false}
	} else if data.Password == "" {
		return util.RS{Message: "請輸入密碼", Status: false}
	} else if status, id := users.CheckActAndPwd(data.Account, data.Password); !status {
		return util.RS{Message: "帳號或密碼錯誤", Status: false}
	} else {
		t := time.Now()                  //取得現在時間
		tt := t.Format("20060102150405") //時間格式設定
		tmp := strconv.Itoa(id) + ";" + tt
		token, _ := crypto.KeyEncrypt(tmp)
		return util.RS{Message: token, Status: true}
	}
}

