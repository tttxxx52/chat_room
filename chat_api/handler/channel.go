package handler

import (
	"api/content"
	"api/model"
	"api/util"
	"encoding/json"
	"github.com/gin-gonic/gin"
)

type ChannelHandler content.Handler

func (handler *ChannelHandler) GetChannelId(c *gin.Context) interface{} {
	type dataReceived struct {
		ToUserId int `json:"toUserId"`
	}
	var data dataReceived
	var channel model.Channel
	if err := json.Unmarshal([]byte(c.GetString("parameters")), &data); err != nil {
		return util.RS{Message: "", Status: false}
	} else if data.ToUserId == 0 {
		return util.RS{Message: "", Status: false}
	} else {
		return channel.GetChannelId(c.GetInt("userId"), data.ToUserId)
	}
}
