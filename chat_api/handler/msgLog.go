package handler

import (
	"api/content"
	"api/model"
	"api/util"
	"api/util/log"
	"encoding/json"
	"github.com/gin-gonic/gin"
)

type MsgLogHandler content.Handler

func (handler *MsgLogHandler) GetMsgLog(c *gin.Context) interface{} {
	type dataReceived struct {
		RoomId string `json:"roomId"`
	}
	var msgLog model.MsgLog
	var data dataReceived

	if err := json.Unmarshal([]byte(c.GetString("parameters")), &data); err != nil {
		return util.RS{Message: "", Status: false}
	} else {
		return msgLog.GetRoomMsg(data.RoomId, c.GetInt("userId"))
	}
}

func (handler *MsgLogHandler) GetUnReadCountList(c *gin.Context) interface{} {
	var unReadCount model.UnReadCount
	if c.GetInt("userId") == 0 {
		return util.RS{Message: "", Status: false}
	} else {
		return unReadCount.GetUnReadCountList(c.GetInt("userId"))
	}
}

func (handler *MsgLogHandler) ReadMessage(c *gin.Context) interface{} {
	type dataReceived struct {
		ToUserId int `json:"toUserId"`
	}
	var data dataReceived
	var unread model.UnReadCount
	if err := json.Unmarshal([]byte(c.GetString("parameters")), &data); err != nil {
		return util.RS{Message: "", Status: false}
	} else {
		log.Error(unread.UpdateReadCount(data.ToUserId, c.GetInt("userId"), 0))
		return util.RS{Message: "", Status: true}
	}
}
