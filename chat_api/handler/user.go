package handler

import (
	"api/content"
	"api/model"
	"github.com/gin-gonic/gin"
)

type UserHandler content.Handler

func (handler *UserHandler) GetFriendList(c *gin.Context) interface{} {
	var friendList model.FriendList
	return friendList.GetFriendList(c.GetInt("userId"))
}
