package main

import (
	"api/config"
	"api/content"
	"api/database/mysql"
	"api/factory"
	"api/service"
	"api/util"
	"api/util/log"
	"api/websocket"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"net/http"
	"reflect"
)

func init() {
	gin.SetMode(gin.ReleaseMode)
	// 初始化設定
	config.InitConfig()
}

func main() {

	//DataBase Pool
	mysql.OpenConnect()

	router := gin.Default()
	router.Use(cors.New(cors.Config{
		AllowAllOrigins: true,
		AllowMethods:    []string{"GET", "POST"},
		AllowHeaders:    []string{"Content-Type", "Auth-Token", "Csrf-Token", "Access-Control-Allow-Origin"},
	}))
	apiRouter := router.Group("/api")
	apiRouter.Use(service.AuthToken())
	{
		apiRouter.POST("/auth", api)
		apiRouter.POST("", api)

	}
	// webSocket
	router.GET("/ws/connect", websocket.Connect)
	_ = router.Run(":6101")
}


func api(c *gin.Context) {
	var context content.Context
	newFactory := factory.ActionFactoryAuth
	userId := c.GetInt("userId")
	if err := c.ShouldBindJSON(&context); err != nil {
		log.Error(err)
		c.JSON(http.StatusOK, util.RS{Message: "should bind JSON error", Status: false})
		return
	} else if a, ok := newFactory[context.Action]; ok && userId <= 0 {
		c.JSON(http.StatusOK, util.RS{Message: "api auth failure", Status: false})
		return
	} else {
		if !ok {
			a = factory.ActionFactory[context.Action]
		}

		c.Set("parameters",context.Parameters)
		v := make([]reflect.Value, 0)
		v = append(v, reflect.ValueOf(c))

		rd := reflect.ValueOf(a).MethodByName(context.Action).Call(v)

		if len(rd) > 0 {
			c.SecureJSON(http.StatusOK, rd[0].Interface())
			return
		}
	}
}

