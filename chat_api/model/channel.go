package model

import (
	"api/database/mysql"
	"fmt"
	"github.com/satori/go.uuid"
)

type Channel struct {
	Id      int    `table:"id"`
	UserId1 int    `table:"user_id_1"`
	UserId2 int    `table:"user_id_2"`
	Uuid    string `table:"uuid"`
}


func (model *Channel) GetChannelId(userId, toUserId int) string {
	var myUuid string
	mysql.NewDB().Option(func(database mysql.Database) {
		var value []interface{}
		sqlString := `SELECT uuid FROM channel WHERE (user_id_1 = ? AND user_id_2 = ? ) OR (user_id_1 = ? AND user_id_2 = ? ) `

		value = append(value, userId, toUserId, toUserId, userId)
		err := database.QueryRow(sqlString, value...).Scan(&myUuid)

		if err != nil {
			u1 := uuid.Must(uuid.NewV4(), nil)
			u2:=fmt.Sprintf("%s", u1)
			model.UserId1 = userId
			model.UserId2 = toUserId
			model.Uuid = u2
			myUuid = u2
			_,err :=mysql.Model(model).Insert()
			if err != nil {
				myUuid = ""
			}
		}
		return
	})
	return myUuid
}
