package model

import (
	"api/database/mysql"
	"api/util/log"
	"database/sql"
)

type FriendList struct {
	Id           int `table:"id"`
	UserId       int `table:"user_id"`
	FriendUserId int `table:"friend_user_id"`
}

func (model *FriendList) GetFriendList(userId int) []map[string]interface{} {
	dataList := make([]map[string]interface{}, 0)
	var friendUserId int
	var name string
	mysql.Model(model).
		InnerJoin("users", "users.id", "=", "friend_list.friend_user_id").
		Where("friend_list.user_id", "=", userId).
		Select([]string{"friend_list.friend_user_id", "users.name"}).
		Get(func(rows *sql.Rows) {
			log.Error(rows.Scan(&friendUserId, &name))
			data := map[string]interface{}{
				"id":   friendUserId,
				"name": name,
			}
			dataList = append(dataList, data)
		})
	return dataList
}
