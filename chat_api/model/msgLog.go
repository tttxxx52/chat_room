package model

import (
	"api/database/mongo"
	"api/util/log"
	"context"
	"go.mongodb.org/mongo-driver/bson"
)

type MsgLog struct {
	Message string `json:"message"`
	UserId  int    `json:"userId"`
}

func (model *MsgLog) GetRoomMsg(roomId string, userId int) (dataList []map[string]interface{}) {
	mongo.NewMongo(func(handler *mongo.MongoHandler) {
		type MessageLogs struct {
			RoomId string
			Data   []MsgLog
		}
		var messageLogs MessageLogs

		filter := bson.M{"roomId":roomId}
		if err := handler.Collection("message_storage").FindOne(context.Background(), filter).Decode(&messageLogs); err == nil && len(messageLogs.Data) > 0 {
			var msgType string
			for _, item := range messageLogs.Data {
				msgType = "myself"
				if item.UserId != userId{
					msgType = ""
				}
				data := map[string]interface{}{
					"message":item.Message,
					"userId":item.UserId,
					"msgType":msgType,
				}

				dataList = append(dataList, data)
			}
		}
	})
	return dataList
}
func (model *MsgLog) PushNewMsg(roomId, message string, userId int) error {
	var err error
	mongo.NewMongo(func(handler *mongo.MongoHandler) {
		type MessageLogs struct {
			RoomId string
			Data   []MsgLog
		}
		var messageLogs MessageLogs
		filter := bson.M{"roomId":roomId}
		if err := handler.Collection("message_storage").FindOne(context.Background(), filter).Decode(&messageLogs); err == nil && len(messageLogs.Data) > 0 {
			_, err = handler.Collection("message_storage").UpdateOne(
				context.Background(),
				bson.M{"roomId": roomId},
				bson.M{"$push": bson.M{"data": bson.M{"message": message, "userId": userId}}},
			)
		}else{
			dataList:=make([]map[string]interface{},0)
			data:=map[string]interface{}{
				"message":message,
				"userId":userId,
			}
			dataList = append(dataList,data )

			insertDB:=map[string]interface{}{
				"roomId":roomId,
				"data":dataList,
			}

			_, err = handler.Collection("message_storage").InsertOne(context.Background(), insertDB)
		}

		if err != nil {
			log.Error(err)
		}
	})
	return err
}
