package model

import (
	"api/database/mongo"
	"api/util/log"
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"context"
	"go.mongodb.org/mongo-driver/bson"
)

type UnReadCount struct {
	Id            string `bson:"_id" json:"_id"`
	SendUserId    int    `json:"sendUserId"`
	ReceiveUserId int    `json:"receiveUserId"`
	Count         int    `json:"count"`
}

func (model *UnReadCount) GetUnReadCountList(userId int) (dataList []map[string]interface{}) {
	dataList = make([]map[string]interface{}, 0)
	mongo.NewMongo(func(handler *mongo.MongoHandler) {
		cursor, err := handler.Collection("message_unread_count").Find(context.Background(), bson.M{"receiveUserId": userId})
		if err != nil {
			log.Error(err)
		}
		for cursor.Next(context.Background()) {
			var unReadCount UnReadCount
			if err = cursor.Decode(&unReadCount); err != nil {
				log.Error(err)
			}

			data := map[string]interface{}{
				"sendUserId":    unReadCount.SendUserId,
				"receiveUserId": unReadCount.ReceiveUserId,
				"count":         unReadCount.Count,
			}
			dataList = append(dataList, data)
		}
	})
	fmt.Println(dataList)
	return dataList
}

func (model *UnReadCount) UpdateReadCount(sendUserId, receiveUserId, count int) error {
	var err error
	mongo.NewMongo(func(handler *mongo.MongoHandler) {
		var unReadCount UnReadCount

		err = handler.Collection("message_unread_count").
			FindOne(context.Background(), bson.M{"sendUserId": sendUserId, "receiveUserId": receiveUserId}).
			Decode(&unReadCount)

		if unReadCount.Id != "" {
			id, _ := primitive.ObjectIDFromHex(unReadCount.Id)
			_, err = handler.Collection("message_unread_count").UpdateOne(context.Background(),
				bson.M{"_id": id},
				bson.M{"$set": bson.M{"count": count}},
			)
		} else {
			insertDB := map[string]interface{}{
				"sendUserId":    sendUserId,
				"receiveUserId": receiveUserId,
				"count":         count,
			}

			_, err = handler.Collection("message_unread_count").InsertOne(context.Background(), insertDB)
		}

		if err != nil {
			log.Error(err)
		}
	})
	return err
}

func (model *UnReadCount) GetUnReadCount(sendUserId, receiveUserId int) int {
	var unReadCount UnReadCount
	var err error
	mongo.NewMongo(func(handler *mongo.MongoHandler) {
		err = handler.Collection("message_unread_count").
			FindOne(context.Background(), bson.M{"sendUserId": sendUserId, "receiveUserId": receiveUserId}).
			Decode(&unReadCount)
	})
	if err != nil {
		return 0
	} else {
		return unReadCount.Count
	}
}
