package model

import (
	"api/database/mysql"
	"api/util/crypto"
	"api/util/log"
)

type Users struct {
	Id          int       `table:"id"`
	Account     string    `table:"account"`
	Password    string    `table:"password"`
	Name        string    `table:"name"`
}

func (model *Users) CheckActAndPwd(account, password string) (bool, int) {
	var id int
	var status bool
	var dbPassword string

	log.Error(mysql.Model(model).
		Where("account", "=", account).
		Select([]string{"id", "password"}).
		Find().
		Scan(&id, &dbPassword))



	dbPassword, _ = crypto.KeyDecrypt(dbPassword)
	if password == dbPassword {
		status = true
	} else {
		status = false
		id = 0
	}

	return status, id
}