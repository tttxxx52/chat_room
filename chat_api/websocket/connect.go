package websocket

import (
	"api/model"
	"api/util/crypto"
	"api/util/log"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"net/http"
	"strconv"
	"strings"
)

const (
	Join    = "join"
	Message = "message"
	Leave   = "leave"
	Create  = "create"
)

var playRoom = make(map[string]map[*websocket.Conn]int)
var userSocketConn = make(map[int]*websocket.Conn)

type Msg struct {
	Action   string `json:"action"`
	Message  string `json:"message"`
	RoomId   string `json:"roomId"`
	Token    string `json:"token"`
	UUID     string `json:"uuid"`
	ToUserId int    `json:"toUserId"`
}

var wsUpgrade = websocket.Upgrader{
	ReadBufferSize:  4096,
	WriteBufferSize: 4096,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

//初始化連線
func wsWithFunction(c *gin.Context, f func(conn *websocket.Conn)) {
	conn, err := wsUpgrade.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		log.Error(err)
		return
	}
	go f(conn)
}

//對應連線推播
func Send(sendUserId, receiveUserId, count int) {
	returnMsg := map[string]interface{}{
		"action":     "unRead",
		"sendUserId": sendUserId,
		"count":      count,
	}
	if _, ok := userSocketConn[receiveUserId]; ok {
		err := userSocketConn[receiveUserId].WriteJSON(returnMsg)
		if err != nil {
			log.Error(err)
		}
	}
}

//對應房間推播
func Broadcast(playRoom map[*websocket.Conn]int, msg Msg, userId int) {
	fmt.Println("public Room Broadcast")
	for k, _ := range playRoom {
		returnMsg := map[string]interface{}{
			"uuid":    msg.UUID,
			"action":  msg.Action,
			"message": msg.Message,
		}
		err := k.WriteJSON(returnMsg)
		if err != nil {
			log.Error(err)
			k.Close()
			delete(playRoom, k)
		}
	}
}

func Connect(c *gin.Context) {
	fmt.Println("連線")
	wsWithFunction(c, func(conn *websocket.Conn) {
		for {
			var userId int
			var msg Msg
			// receive 訊息
			_, message, err := conn.ReadMessage()
			if err != nil {
				conn.Close()
				delete(playRoom[msg.RoomId], conn)
				for key, value := range userSocketConn {
					if value == conn{
						delete(userSocketConn, key)
						break
					}
				}
				break
			}
			json.Unmarshal(message, &msg)
			id, status := ParseToken(msg.Token)
			if status {
				userId = int(id)
			} else {
				return
			}


			switch msg.Action {
			case Join:
				if _, ok := playRoom[msg.RoomId]; !ok {
					// 房間不存在
					mm := make(map[*websocket.Conn]int)
					mm[conn] = userId
					playRoom[msg.RoomId] = mm
				} else {
					playRoom[msg.RoomId][conn] = userId
				}

				var msgLog model.MsgLog
				dataList, _ := json.Marshal(msgLog.GetRoomMsg(msg.RoomId, userId))
				msg.Message = string(dataList)
				Broadcast(playRoom[msg.RoomId], msg, userId)
				break
			case Message:
				msg.Action = "message"

				// 如果房間數未滿兩個人count+1
				if len(playRoom[msg.RoomId]) < 2 {
					var unread model.UnReadCount
					count := unread.GetUnReadCount(userId, msg.ToUserId)
					count += 1
					log.Error(unread.UpdateReadCount(userId, msg.ToUserId, count))
					Send(userId, msg.ToUserId, count)
				}
				Broadcast(playRoom[msg.RoomId], msg, userId)

				var msgLog model.MsgLog
				log.Error(msgLog.PushNewMsg(msg.RoomId, msg.Message, userId))
				break
			case Leave:
				delete(playRoom[msg.RoomId], conn)
			case Create:
				userSocketConn[userId] = conn
			}

			conn.SetCloseHandler(func(code int, text string) error {
				fmt.Println("斷線拉")
				delete(playRoom[msg.RoomId], conn)
				for key, value := range userSocketConn {
					if value == conn{
						delete(userSocketConn, key)
						break
					}
				}

				return nil
			})

		}
	})
}




func ParseToken(token string) (int64, bool) {
	if token == "" {
		return 0, false
	}

	tokenInfo, err := crypto.KeyDecrypt(token)
	if err != nil {
		log.Error(err)
		return 0, false
	}

	spiltStr := strings.Split(tokenInfo, ";")

	if len(spiltStr) == 2 {
		userId := spiltStr[0]
		//local, err := time.LoadLocation("Asia/Taipei")

		//if err != nil {
		//	log.Error(err)
		//	return 0, false
		//}

		//t, err := time.ParseInLocation("20060102150405", spiltStr[1], local)
		//if err != nil {
		//	log.Error(err)
		//	return 0, false
		//}

		//// 檢查 token 時效
		//if util.TimeNow().Sub(t).Hours() >= 24 {
		//	return 0, false
		//}

		userId64, err := strconv.ParseInt(userId, 10, 64)
		if err != nil {
			log.Error(err)
			return 0, false
		}

		return userId64, true
	} else {
		return 0, false
	}
}
