package websocket

import (
	"api/content"
	"api/util/log"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"time"
)

type WebsocketExample content.Websocket

func (example *WebsocketExample) Test() {
	exitFlag := false

	for !exitFlag {
		//接收資料
		_, message, err := example.Conn.ReadMessage()
		if err != nil {
			log.Error(err)
			exitFlag = true
		}
		msg := string(message[:])
		fmt.Println(msg)

		////////////////////////////////////
		///			do something 		 ///
		///	  	 your business logic 	 ///
		////////////////////////////////////

		//回應資料
		var sendJson []interface{}
		b, _ := json.Marshal(sendJson)
		t := websocket.TextMessage
		err = example.Conn.WriteMessage(t, b)


		if err != nil {
			log.Error(err)
			exitFlag = true
		}

		time.Sleep(1 * time.Second)
	}
}