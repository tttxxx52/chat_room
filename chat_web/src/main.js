import Vue from 'vue'
import App from './App.vue'
import router from './router'
import {store} from './store/'


// ===================================== 外部套件 =====================================
//---- jQuery
import {jQuery} from 'jquery'

window.$ = jQuery;
window.JQuery = jQuery;

//---- Bootstrap
import Bootstrap from 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

//---- MainCSS
import '../src/assets/css/style.css'
import '../src/assets/css/fontawesome/fontawesome.css'

//---- ElementUI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(ElementUI);

//---- VueCarousel
import VueCarousel from 'vue-carousel';

Vue.use(VueCarousel);

//--- Public
import Public from './util/public'

Public.install = function (Vue) {
    Vue.prototype.$public = Public;
};
Vue.use(Public);

//--- Http
import Http from './util/http'

Http.install = function (Vue) {
    Vue.prototype.$http = Http;
};
Vue.use(Http);

//複製用
import Clipboard from 'v-clipboard';

Vue.use(Clipboard)


//---- Auth
import Auth from './util/auth'

Auth.install = function (Vue) {
    Vue.prototype.$auth = Auth;
};
Vue.use(Auth);

Vue.config.productionTip = false;

//--- VEditor
import VEditor from './util/editor'

//列印功能
import Print from 'vue-print-nb'
Vue.use(Print); //註冊

new Vue({
    Bootstrap,
    store,
    router,
    VEditor,
    render: h => h(App)
}).$mount('#app')
