import Vue from 'vue'
import Router from 'vue-router'

import Login from './views/Login.vue'
import MainPage from './views/MainPage.vue'


Vue.use(Router);

export default new Router({
    mode: 'history',
    base: '/',
    routes: [
        //後台登入
        {
            path: '/',
            name: 'Login',
            component: Login
        },
        {
            path: '/chat',
            name: 'MainPage',
            component: MainPage,
        }
    ]
})

