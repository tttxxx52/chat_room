import Vue from 'vue';
import Vuex from 'vuex';
import state from './state';

//import app from "../main";

Vue.use(Vuex);
export const store = new Vuex.Store({
    strict: true,
    state,
    mutations: {
        updateComprehensiveList(state, data) {
            state.comprehensive.list = data.list;
        },
        clearComprehensiveList(state) {
            state.comprehensive.list = [];
        },
    },
    actions: {}
})
