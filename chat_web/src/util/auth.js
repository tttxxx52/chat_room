/* eslint-disable semi,no-trailing-spaces,indent,quotes,space-infix-ops,comma-dangle,padded-blocks,no-unused-vars,eol-last,semi-spacing */


let admin_token = window.localStorage.getItem('rc_clinic_admin_token');


export default {
    isAdminLogin() {
        return admin_token !== null && admin_token !== '';
    },
    setAdminToken(t) {
        window.localStorage.setItem('rc_clinic_admin_token', t + '');
        admin_token = t;
    },
    getAdminToken() {
        return admin_token;
    },
    clearToken() {
        admin_token = '';
        window.localStorage.removeItem('rc_clinic_admin_token');
    },
}
