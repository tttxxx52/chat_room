const prod = process.env.NODE_ENV === "production";

module.exports = {
    devServer:{
        disableHostCheck:true,
    },

    publicPath: "/",
    productionSourceMap:  !prod,
    configureWebpack: config => {
        config.externals = {
            './cptable': 'var cptable',
            '../xlsx.js': 'var _XLSX'
        }
    },
};
